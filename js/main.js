$(document).ready(function() { 
	// var widget = new PaintingWidget();
    $('#pwidget').pWidget({ 
    	theme: 'light', 
    	dimensions: { 
    		width: 400, 
    		height: 400
    	},
    });

    $('#pwidget2').pWidget({
    	fonts: ['open sans','arial','monotype corsiva'],
    	dimensions: { 
    		width: 500, 
    		height: 300
    	},
    	canvasColor: "#ABABAB",
    	basicTools: true,
    });
});