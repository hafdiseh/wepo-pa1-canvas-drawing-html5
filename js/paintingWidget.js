/* Painting widget
 * Dependencies: font awesome, jquery, base.js, spectrum
 */
;(function($, window, document, undefined) {
    var PaintingWidget = Base.extend({
        options: {
            canvasID: 'pwidget-canvas',
            textBoxID: '',
            dimensions: {
                width: 800,
                height: 600,
            },
            icons: {
                pencil: 'fa-paint-brush',
                circle: 'fa-circle-o',
                rect: 'fa-square-o',
                line: 'fa-minus',
                select: 'fa-arrows-alt',
                undo: 'fa-arrow-left',
                redo: 'fa-arrow-right',
                clear: 'fa-trash-o',
                save: 'fa-floppy-o',
                load: 'fa-folder-open-o',
                export: 'fa-share-square-o',
                template: 'fa-image',
            },
            fonts: ['georgia', 'times new roman', 'verdana'],
            basicTools: false,
            theme: 'dark',
            canvasColor: '#ffffff',
        },
        constructor: function(options) {
            this.setOpts(options);
            this.history = [];
            this.undohistory = [];
            var opts = this.options;
            var toolObjects = {};
            this.toolObjects = toolObjects;
            this.load = {};
            this.save = {};
            this.template = {};

            /********************
             * HTML element setup
             *******************/ 
            (function(self) {
                var tools;
                //Local variables for jquery elements
                var htmlCanvas;
                var toolDiv;
                var formatToolDiv;
                var formatTools;
                var canvasToolDiv;
                var canvasTools;

                var generateCanvasTools = function() {
                    canvasTools = {
                        'undo': 'Undo',
                        'redo': 'Redo',
                        'clear': 'Clear',
                    };
                    //load, save and export are part of the
                    //"advanced" toolset, which can be toggled off
                    //via options when creating the painting widget
                    if(!opts.basicTools) {
                        canvasTools.load = 'Load';
                        canvasTools.save = 'Save';
                        canvasTools.template = 'Load template';
                        canvasTools.export = 'Export';
                    }
                    $.each(canvasTools, function(name, text) {
                        var toolButton = $('<span></span>',
                        {
                            'class': 'fa pwidget-button ' + opts.theme,
                            'title': text,
                        });
                        toolObjects[name] = toolButton;
                        //if a css font icon is defined in the options,
                        //add the class, otherwise, text.
                        if(opts.icons[name]) {
                            toolButton.addClass(opts.icons[name]);
                        } else {
                            toolButton.html(text);
                        }
                        toolButton.appendTo(canvasToolDiv);
                    });
                };

                var generateToolbarButtons = function() {
                    $.each(tools, function(_, tool) {
                        var toolButton = $('<span></span>',
                        {
                            'id': 'pwidget-' + tool.type,
                            'title': tool.name + ' tool',
                            'class': 'fa pwidget-tool pwidget-tool-inactive ' + 
                                     opts.theme,
                        });
                        toolObjects[tool.type] = toolButton;
                        //if an icon is set for the tool type,
                        //use the icon
                        if(opts.icons[tool.type]) {
                            toolButton.addClass(opts.icons[tool.type]);
                        } else {
                            //otherwise fall back to the tool name
                            toolButton.html(tool.name);
                        }
                        toolButton.appendTo(toolDiv);
                    });
                };

                var generateFormattingOptions = function() {
                    formatTools = {
                        'stroke-color': {
                            'type': 'text'
                        },
                        'fill-color': {
                            'type': 'text'
                        },
                        'text-color': {
                            'type': 'text'
                        },
                        'stroke-thickness': {
                            'type': 'number',
                            'value': 1,
                            'min': 1,
                            'max': 50
                        },
                        'text-font': {
                            'type': 'select',
                            'options': opts.fonts
                        },
                        'text-size': {
                            'type': 'number',
                            'value': 12,
                            'min': 8,
                            'max': 72,
                            'step': 2
                        },
                        'text-string': {
                            'type': 'text',
                            'placeholder': 'Enter text here...'
                        },
                        'template-string': {
                            'type': 'text',
                            'placeholder': 'Enter template name...'
                        },
                        'template-save': {
                            'type': 'button',
                            'value': 'Save as template'
                        },

                    };
                    $.each(formatTools, function(toolName, props) {
                        var formatToolButton;
                        if(toolName === 'text-font') {
                            formatToolButton = $(
                                '<select></select>', {
                                    'class': 'pwidget-' +
                                        toolName + ' ' + opts.theme
                                });
                            $.each(props.options, function(key, value) {
                                $('<option></option>', {
                                    'value': value,
                                    'html': value,
                                    'style': 'font-family: '+value,
                                }).appendTo(formatToolButton);
                            });
                        } else {
                            formatToolButton = $(
                                '<input></input>', {
                                    'class': 'pwidget-' +
                                        toolName + ' ' + opts.theme
                                });
                            $.each(props, function(key, value) {
                                formatToolButton.attr(key,
                                    value);
                            });
                        }
                        //hide the tools initially
                        toolObjects[toolName] = formatToolButton.hide();
                        formatToolButton.appendTo(formatToolDiv);
                    });
                };

                var setupSpectrumWidgets = function () {
                    //initialise spectrum color select widgets
                    toolObjects['stroke-color'].spectrum({
                        color: '#000',
                        showPalette: true,
                        showButtons: false,
                        allowEmpty: true
                    });
                    toolObjects['fill-color'].spectrum({
                        showPalette: true,
                        showButtons: false,
                        allowEmpty: true
                    });
                    toolObjects['text-color'].spectrum({
                        showPalette: true,
                        showButtons: false,
                        color: '#000'
                    });
                    //hide the widgets
                    toolObjects['stroke-color-spectrum-widget'] = toolObjects[
                        'stroke-color'].next('div').hide();
                    toolObjects['fill-color-spectrum-widget'] = toolObjects[
                        'fill-color'].next('div').hide();
                    toolObjects['text-color-spectrum-widget'] = toolObjects[
                        'text-color'].next('div').hide();
                };

                self.canvas = $('<canvas></canvas>',
                {
                    'height': opts.dimensions.height.toString(),
                    'width': opts.dimensions.width.toString(),
                    'id': opts.canvasID,
                    'class': 'pwidget-canvas ' + opts.theme,
                    'html': '<p>Unfortunately, your browser is not supported</p>',
                    'tabindex': '-1'
                }).css('cursor', 'crosshair')
                  .css('background-color', opts.canvasColor);
                $(opts.containerID).addClass("pwidget " + opts.theme)
                                            .width(self.canvas.width());
                htmlCanvas = self.canvas[0];
                htmlCanvas.width = self.canvas.width();
                htmlCanvas.height = self.canvas.height();
                self.context = htmlCanvas.getContext('2d');
                self.tools = {
                    pencil: new Pencil(self.context, htmlCanvas),
                    circle: new Circle(self.context, htmlCanvas),
                    line: new Line(self.context, htmlCanvas),
                    rect: new Rect(self.context, htmlCanvas),
                    text: new Text(self.context, htmlCanvas),
                };
                tools = self.tools;

                if(!opts.basicTools) {
                    //the select tool needs a reference
                    //to the tools, and therefore is added separately
                    tools.select = new Select(self.canvas, self.history, tools);
                }

                //HTML generation for the top bar containing
                //the drawing tools
                toolDiv = $('<div></div>',
                {
                    'class': 'pwidget-toolbox ' + opts.theme,
                });
                generateToolbarButtons();
                toolDiv.appendTo(opts.containerID);

                //Formatting tool html generation
                formatToolDiv = $('<div></div>',
                {
                    'class': 'pwidget-formatbox ' + opts.theme,
                });
                generateFormattingOptions();
                formatToolDiv.appendTo(opts.containerID);
                setupSpectrumWidgets();

                //Bottom button pane generation
                canvasToolDiv = $('<div></div>',
                {
                    'class': 'pwidget-canvasbox ' + opts.theme,
                });
                generateCanvasTools();
                self.canvas.appendTo(opts.containerID);
                canvasToolDiv.appendTo(opts.containerID);

                //the load, save and template drawers
                if(!opts.basicTools) {
                    self.load.div = $('<div></div>',
                    {
                        'class': 'pwidget-loadbox ' + opts.theme
                    });

                    self.load.select = $('<select></select>',
                    {});

                    $.each(['load', 'save', 'template'], function(_, value){
                        self[value].button = $('<span></span>',
                        {
                            'class': 'pwidget-button ' + opts.theme,
                            'html': 'Confirm',
                        });

                    });
                    self.load.select.appendTo(self.load.div);
                    self.load.button.appendTo(self.load.div);

                    self.save.div = $('<div></div>',
                    {
                        'class': 'pwidget-savebox ' + opts.theme
                    });

                    self.save.title = $('<input>',
                    {
                        'type': 'text',
                        'placeholder': 'Enter title of drawing',
                    });

                    self.save.title.appendTo(self.save.div);
                    self.save.button.appendTo(self.save.div);

                    self.template.div = $('<div></div>',
                    {
                        'class': 'pwidget-loadbox ' + opts.theme
                    });

                    self.template.select = $('<select></select>',
                    {});

                    self.template.select.appendTo(self.template.div);
                    self.template.button.appendTo(self.template.div);

                    self.load.div.appendTo(opts.containerID);
                    self.load.div.hide();
                    self.save.div.appendTo(opts.containerID);
                    self.save.div.hide();
                    self.template.div.appendTo(opts.containerID);
                    self.template.div.hide();
                }
            
                self.container = htmlCanvas.parentNode;
            })(this);

            this.format = {
                stroke: {
                    color: '#000000',
                    lineWidth: 1
                },
                fill: {
                    color: false
                },
                text: {
                    font: 'georgia',
                    size: 12,
                    text: '',
                    shadow: false,
                    color: '#000000'
                },
            };

            this.setTool({
                data: this.tools.pencil
            });
            this.setTextFont();
            this.setTextColor();
            this.setTextSize();
            this.setStrokeColor();
            this.setFillColor();

            //Binding UI events
            (function(self) {
                var canvas = self.canvas;
                var toolObjects = self.toolObjects;
                var bindTools = function(tool, toolObj) {
                    toolObjects[tool].mouseup(toolObj, self.setTool.bind(
                        self));
                };
                canvas.mousedown(self.eventProxy.bind(self));
                canvas.mousemove(self.eventProxy.bind(self));
                canvas.mouseup(self.eventProxy.bind(self));
                canvas.bind('selectstart dragstart', function(ev) {
                        ev.preventDefault();
                        return false;
                    }, false);
                canvas.keydown(self.keyboardProxy.bind(self));
                canvas.on('wheel', self.mousescroll.bind(self));
                toolObjects['stroke-color'].on('change',
                                       self.setStrokeColor.bind(self));
                toolObjects['fill-color'].on('change',
                                     self.setFillColor.bind(self));
                toolObjects['text-color'].on('change',
                                     self.setTextColor.bind(self));
                toolObjects['stroke-thickness'].on('change',
                                           self.setStrokeThickness.bind(self));
                toolObjects['text-size'].on('change',
                                    self.setTextSize.bind(self));
                toolObjects['text-string'].on('change',
                                     self.setText.bind(self));
                toolObjects['text-font'].on('change',
                                     self.setTextFont.bind(self));

                toolObjects.undo.mouseup(self.undo.bind(self));
                toolObjects.redo.mouseup(self.redo.bind(self));
                toolObjects.clear.mouseup(self.clear.bind(self));

                if(!self.options.basicTools) {
                    toolObjects['template-save'].mouseup(self.doSaveTemplate.bind(self)); 

                    toolObjects.export.click(self.export.bind(self));
                    toolObjects.load.click(self.loadClick.bind(self));
                    toolObjects.save.click(self.saveClick.bind(self));
                    toolObjects.template.click(self.templateClick.bind(self));
                    self.save.button.click(self.doSave.bind(self));
                    self.template.button.click(self.doLoadTemplate.bind(self));
                    self.load.button.click(self.doLoad.bind(self));
                }
                $.each(self.tools, function(key, value) {
                    bindTools(key, value);
                });
            })(this);
        },
        redraw: function(noclear) {
            var canvas = this.canvas[0];
            var history = this.history;
            if (!noclear) {
                this.context.clearRect(0, 0, canvas.width,
                                       canvas.height);
            }
            for (var i = 0; i < history.length; i++) {
                var elem = history[i];
                if (elem.deleted){
                    history.splice(i, 1);
                    i--;
                } else {
                    var tool = this.tools[elem.type];
                    tool.draw.call(tool, elem);
                }
            }
        },
        setOpts: function(options) {
            this.options = $.extend({}, this.options, options || {});
        },
        /**********************************************************
         * UI FUNCTION SECTION                                    *
         **********************************************************/
        /*
         * These functions get the color values from the spectrum widgets
         * and set them in the formatting options
         */        
        setStrokeColor: function() {
            var stroke = this.format.stroke;
            stroke.color = this.toolObjects['stroke-color']
                .spectrum('get');
            if (stroke.color) {
                stroke.color = stroke.color.toHexString();
            }
        },
        setFillColor: function() {
            var fill = this.format.fill;
            fill.color = this.toolObjects['fill-color'].spectrum('get');
            if (fill.color) {
                fill.color = fill.color.toHexString();
            }
        },
        /* 
         * Also sets the css of the input field to the selected color
         * to preview the color on the text.
         */
        setTextColor: function() {
            var text = this.format.text;
            text.color = this.toolObjects['text-color'].spectrum('get');
            if (text.color) {
                text.color = text.color.toHexString();
                this.toolObjects['text-string'].css('color', text.color);
            } else {
                this.toolObjects['text-string'].css('color', '#000');
            }
        },
        /*
         * Sets the text to the current value of the input field
         */
        setText: function(ev) {
            this.format.text.string = ev.target.value;
        },
        /*
         * Sets the format font to the currently selected font
         * in the dropdown. Also sets the font of the input field
         * to preview the text.
         */
        setTextFont: function() {
            this.format.text.font = this.toolObjects['text-font'].css('font-family', 
                this.toolObjects['text-font'].val()).val();
            this.toolObjects['text-string'].css('font-family', this.toolObjects['text-font'].val());
        },
        /*
         * Sets the format text size to the value of the range input, 
         * and makes sure it is clamped within a certain range.
         */
        setTextSize: function() {
            var text = this.format.text;
            text.size = this.toolObjects['text-size'].val();
            if (text.size < 8) {
                text.size = 8;
            }
            if (text.size > 72) {
                text.size = 72;
            }
        },
        /*
         * Same as the previous, but for stroke thickness.
         */
        setStrokeThickness: function() {
            var stroke = this.format.stroke;
            stroke.lineWidth = this.toolObjects['stroke-thickness'].val();
            if (stroke.lineWidth < 1) {
                stroke.lineWidth = 1;
            }
            if (stroke.lineWidth > 50) {
                stroke.lineWidth = 50;
            }
        },
        /*
         * Change the tool and update the UI.
         */
        setTool: function(ev) {
            var toolObjects = this.toolObjects;
            if (this.tool) {
                $.each(this.tool.formatOpts, function(index,
                    value) {
                    toolObjects[value].hide();
                });
                toolObjects[this.tool.type].toggleClass(
                    'pwidget-tool-active pwidget-tool-inactive'
                );
                if (this.tool.type === 'select') {
                    this.tool.clear();
                    this.redraw(false);
                }
            }
            this.tool = ev.data;
            this.canvas.css('cursor', this.tool.cursor);
            toolObjects[ev.data.type].toggleClass(
                'pwidget-tool-active pwidget-tool-inactive'
            );
            $.each(this.tool.formatOpts, function(index, value) {
                toolObjects[value].show()
            });
        },
        /*
         * Undo and redo. Nothing fancy.
         */
        undo: function() {
            if (this.history.length > 0) {
                this.undohistory.push(this.history.pop());
                this.redraw();
            }
        },
        redo: function() {
            if (this.undohistory.length > 0) {
                this.history.push(this.undohistory.pop());
                this.redraw();
            }
        },
        /*
         * Wacky .png export function
         */
        export: function() {
            var dt = this.canvas[0].toDataURL('image/png');
            var download = $('<a></a>',{
                'href': dt,
                'download': 'pWidget-image'
            }).hide();
            this.canvas[0].parentNode.appendChild(download[0]);
            download[0].click();
            this.canvas[0].parentNode.removeChild(download[0]);
        },
        /*
         * Clears the shape history and redraw the canvas.
         */
        clear: function() {
            this.history.length = 0;
            this.undohistory = [];
            this.redraw();
        },
        /*
         * Various UI click handlers.
         * Toggle the visibility of the
         * appropriate divs.
         */
        saveClick: function() {
            this.load.div.hide();
            this.template.div.hide();
            this.save.div.show();
        },
        loadClick: function() {
            this.populateLoad();
            this.save.div.hide();
            this.template.div.hide();
            this.load.div.show();
        },
        templateClick: function() {
            this.populateTemplate();
            this.save.div.hide();
            this.load.div.hide();
            this.template.div.show();
        },
        /*
         * Populate the load dropdown and template
         * dropdown with drawings and templates saved
         * in local storage.
         */
        populateLoad: function() {
            if (window.localStorage['pwidget-drawings']) {
                var img = JSON.parse(window.localStorage['pwidget-drawings']);
                this.load.select.html('');
                var select = this.load.select;
                $.each(img, function(key, value) {
                    var option = $('<option></option>', {
                        'html': value.title,
                        'value': key,
                    });
                    select.append(option);
                });
            }
        },
        populateTemplate: function() {
            if (window.localStorage['pwidget-templates']) {
                var template = JSON.parse(window.localStorage['pwidget-templates']);
                this.template.select.html('');
                var select = this.template.select;
                $.each(template, function(key, value) {
                    var option = $('<option></option>', {
                        'html': value.title,
                        'value': key,
                    });
                    select.append(option);
                });
            }
        },
        /*
         * Saves a template in local storage.
         */
        doSaveTemplate: function() {
            var title = this.toolObjects['template-string'].val();
            if (title && this.tools.select.target.length > 0) {
                var templates = [];
                if (window.localStorage['pwidget-templates']) {
                    templates = JSON.parse(window.localStorage['pwidget-templates']);
                }
                var content = [];
                //if we've already got a group selected we just use that
                if(this.tools.select.target[0].type == 'select') {
                    content = this.tools.select.target[0];
                } else if(this.tools.select.target.length > 1) {
                    //otherwise we need to make a group from our current
                    //selection
                    content = this.tools.select.makeGroup();
                } else {
                    //only one object was selected, cancel.
                    return;
                }
                var saveMe = {
                    'title': templates.length + ' - ' + title,
                    'content': content,
                };
                templates.push(saveMe);
                window.localStorage['pwidget-templates'] = JSON.stringify(templates);
                //some user friendliness, a visual cue that
                //the template was successfully saved
                var button = this.toolObjects['template-save'];
                var originalText = button.val();
                var buttonWidth = button.width();
                button.val('Saved');
                button.width(buttonWidth);
                button.prop('disabled', true);
                this.toolObjects['template-string'].val('');
                setTimeout(function() { 
                    button.val(originalText);
                    button.prop('disabled', false);
                }, 1000);
            }
        },
        /*
         * Saves a drawing in local storage.
         */
        doSave: function() {
            var drawings = [];
            if (window.localStorage['pwidget-drawings']) {
                drawings = JSON.parse(window.localStorage['pwidget-drawings']);
            }
            var title = this.save.title.val();
            if (!title) {
                title = 'untitled';
            }
            this.tools.select.clear();
            var saveMe = {
                'title': drawings.length + ' - ' + title,
                'content': this.history,
            };
            drawings.push(saveMe);
            window.localStorage['pwidget-drawings'] = JSON.stringify(drawings);
            this.save.div.hide();
        },
        /*
         * Loads a drawing from local storage.
         * Replaces current drawing.
         */
        doLoad: function() {
            if (window.localStorage['pwidget-drawings']) {
                var imgs = JSON.parse(window.localStorage['pwidget-drawings']);
                //get the selected drawing
                var img = imgs[this.load.select.val()];
                var history = this.history;
                //if the selected drawing exists
                if (img) {
                    //load it but preserve the history object
                    history.length = 0;
                    this.undohistory.length = 0;
                    $.each(img.content, function(index, value) { 
                        history.push(value);
                    });
                    //and redraw the canvas
                    this.redraw();
                }
            }
            this.load.div.hide();
        },
        /*
         * Loads a template from local storage.
         * Is added to current drawing.
         */
        doLoadTemplate: function() {
            if (window.localStorage['pwidget-templates']) {
                var templates = JSON.parse(window.localStorage['pwidget-templates']);
                var template = templates[this.template.select.val()];
                if (template) {
                    //if the template is found,
                    //we push the object onto the
                    //history list, switch to the select tool
                    //and make sure the loaded template is
                    //the currently selected object.
                    this.undohistory.length = 0;
                    this.history.push(template.content);
                    this.setTool({ data: this.tools.select });
                    this.tools.select.target.length = 0;
                    this.tools.select.target.push(template.content);
                    this.redraw();
                }
            }
            this.template.div.hide();
        },
        /*
         * Allows the use of mouse scroll to
         * increment/decrement line stroke width.
         */
        mousescroll: function(ev) {
            if (ev.shiftKey) {
                if (this.tool.type !== 'select'){
                    ev.preventDefault();
                    var size = this.format.text.size;
                    var delta;
                    if (ev.originalEvent.deltaY === 0){
                        /* Chrome seems to think up and down is deltaX.. */
                        delta = ev.originalEvent.deltaX; 
                    } else {
                        delta = ev.originalEvent.deltaY;
                    }
                    if (this.tool.type === 'text') {
                        if(delta < 0) {
                            if (size < 72) {
                                size++;
                            }
                        } else {
                            if (size > 8) {
                                size--;
                            }
                        }
                        this.format.text.size = size;
                        this.toolObjects['text-size'].val(size);
                    } else {
                        var stroke = this.format.stroke;
                        if(delta < 0) {
                            stroke.lineWidth += 1;
                            if (stroke.lineWidth > 50) {
                                stroke.lineWidth = 50;
                            }
                        } else {
                            stroke.lineWidth -= 1;
                            if (stroke.lineWidth < 1) {
                                stroke.lineWidth = 1;
                            }
                        }
                        this.toolObjects['stroke-thickness'].val(stroke.lineWidth);
                    }
                }
            }
        },
        /*
         * Event handler for tools.
         */
        eventProxy: function(ev) {
            var tool = this.tool;
            var func = tool[ev.type];
            var result;
            if (func) {
                ev._coords = this.getMouseCoords(ev);
                ev._format = $.extend(true, {}, this.format);
                result = func.call(tool, ev);
                if (result) {
                    var canvas = this.canvas[0];
                    var history = this.history;
                    this.context.clearRect(0, 0, canvas.width,
                                           canvas.height);
                    if (!tool.started) {
                        this.undohistory = [];
                        if (Object.keys(result).length) {
                            history.push(result);
                        }
                    }
                    this.redraw(false);
                    if (tool.started) {
                        if (Object.keys(result).length) {
                            tool.draw.call(tool, result);
                        }
                    }
                }
            }
        },
        /*
         * Keyboard handler for shortcuts.
         */
        keyboardProxy: function(ev) {
            switch(ev.keyCode) {
                case 46:
                    if (this.tool.type === 'select') {
                        var target = this.tool.target;
                        var len = target.length;
                        if (len) {
                            for (var i = 0; i < len; i++){
                                target[i].deleted = true;
                            } 
                            this.redraw(false);
                        }
                    }
                    break;
                case 71:
                    if (this.tool.type === 'select'){
                        var tool = this.tool;
                        if (ev.ctrlKey){
                            ev.preventDefault();
                            if (tool.target.length === 1){
                                tool.unGroup();
                            } else {
                                tool.makeGroup();
                            }
                        }
                    }
                    break;
                case 89:
                    if (ev.ctrlKey) {
                        this.redo();
                        ev.preventDefault();
                    }
                    break;
                case 90:
                    if (ev.ctrlKey) {
                        this.undo();
                    }
                    break;
            }

        },
        /*
         * Cross-browser friendly canvas coordinate
         * getter. Hopefully.
         */
        getMouseCoords: function (ev) {
            var x, y;
            var canoffset = this.canvas.offset();
            x = ev.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - Math.floor(canoffset.left);
            y = ev.clientY + document.body.scrollTop + document.documentElement.scrollTop - Math.floor(canoffset.top) + 1;
            ev._x0 = x;
            ev._y0 = y;
        }
    });

/******************************************************************************
 *                                Drawing Tools                               *
 ******************************************************************************
 * Here we define the tools that are used in the painting widget.             *
 * The base Tool class is simply called tool and all other tools extend that  *
 * class including shapes through the Shape class.                            *
 *                                                                            *
 * These are not classes for the shapes themselves, they only define the      *
 * functions that make them and work with them.                               *
 ******************************************************************************/

    var Tool = Base.extend({
        /* Stores a reference to the painting widgets canvas and context */
        constructor: function(context, canvas) {
            this.context = context;
            this.canvas = canvas;
        },
        /* Controlls which format tools this tool has*/
        formatOpts: [
            'stroke-color-spectrum-widget',
            'stroke-thickness'],
        cursor: 'crosshair', //The active cursor while this tool is selected
        points: [], //Stores the coordinates that make up the shape being drawn
        started: false, //Keeps track of if we have started using the tool
        name: '', //The tool name
        type: '', //The tool type
        /* The base functionality of the class on mouse down.
           Takes in the event and stores its coordinates */
        mousedown: function(ev) {
            this.points = [{
                x: ev._x0,
                y: ev._y0
            }, {
                x: ev._x0,
                y: ev._y0
            }];
            this.started = true;
        },
        /* The base functionality of the class on mouse up. It calls
           mousemove one last time, finalizes the object and returns it */
        mouseup: function(ev) {
            this.mousemove(ev);
            this.started = false;
            var result = this.points;
            this.points = [];
            return result;
        },
        /* A definition of mousemove to emphasize that it should be extended */
        mousemove: function() {},
        /* The base functionality of the class for drawing.  It takes care of
           setting style options for child classes */
        draw: function(shape) {
            var options = $.extend(true, {}, shape.format);
            var context = this.context;
            this.context.lineCap = 'round';
            if (options.fill.color) {
                if (options.selected){
                    context.fillStyle = this.getSelectColor(options.fill.color);
                } else {
                    context.fillStyle = options.fill.color;
                }
            }
            if (options.stroke.color) {
                if (options.selected){
                    context.strokeStyle = this.getSelectColor(options.stroke.color);
                } else {
                    context.strokeStyle = options.stroke.color;
                }
            }
            if (options.stroke.lineWidth) {
                context.lineWidth = options.stroke.lineWidth;
            }
            if (options.text.font && options.text.size) {
                context.font = options.text.size + 'px ' +
                    options.text.font;
            }
            return options;
        },
        /* The base functionality of the class for checking if the given 
           x and y coordinates fall within the drawn shape object */
        contains: function(shape, x, y) {
            var canvas = this.canvas;
            var original;
            this.context.clearRect(0, 0, canvas.width, canvas.height);
            if (shape.format.stroke.color) {
                original = shape.format.stroke.lineWidth;
                shape.format.stroke.lineWidth = Math.max(original, 10);
            }
            this.draw(shape);
            var result = this.context.isPointInStroke(x, y);
            if (shape.format.stroke.color) {
                shape.format.stroke.lineWidth = original;
            }
            return result;
        },
        /* The base functionality of the class to move its objects.
           takes a shape along with x and y coordinates and changes
           the shapes coordinates in accordance to the given x and y values.*/
        translate: function(shape, x, y) {
            for (var i = 0; i < shape.points.length; ++i) {
                shape.points[i].x += x;
                shape.points[i].y += y;
            }
        },
        /* The base functionality of the class alter a color string to 
           indicate selection */
        getSelectColor: function(color) {
            var inverse = function (letter) {
                var dec = parseInt(letter, 16);
                dec = dec ^ 8;
                return dec.toString(16);
            };
            var l = color.length;
            return color.substring(0,5) +
                   inverse(color[l - 1]) +
                   inverse(color[l - 2]);
        }
    });

    /*
     * The Select class extends the tool class.  It takes care of
     * highlighting, moving and group/template creation.
     * 
     * Selected items can also be deleted with the delete key.
     */

    var Select = Tool.extend({
        constructor: function(canvas, history, tools) {
            this.history = history; //A reference to the pWidget shape storage
            this.tools = tools; //A reference to the pWidgets active tools
            this.target = [];
            this.canvas = canvas;
            /* Keeps track of if the object has been moved during this
               mousedown event */
            this.moved = false;
            /* Did the mousedown event hit an object? Used for deselecting */
            this.hit = true;
        },
        cursor: 'pointer',
        history: {},
        tools: {},
        name: 'Select',
        type: 'select',
        formatOpts: [
            'template-string',
            'template-save'],
        mousedown: function(ev) {
            this.base(ev); // calls the base class to set points[]
            var tools = this.tools;
            var length = this.history.length;
            var x = this.points[0].x;
            var y = this.points[0].y;
            for (var i = length - 1; i >= 0; --i) {
                /* for every object in this.history */
                var shape = this.history[i];
                if (tools[shape.type].contains(shape, x, y)) {
                    /* We check if the object was clicked */
                    this.canvas.css('cursor', 'move');
                    if (ev.shiftKey){
                        /* If the shift key is held we allow multiple
                           selections */
                        for (var j = 0; j < this.target.length; ++j){
                            if (shape === this.target[j]){
                                return {};
                            }
                        }
                    } else {
                        /* Else we clear out the target to make room for
                           the new one */
                        this.clear(); 
                    }
                    shape.format.selected = true;  //Set the shape as selected
                    this.target.push(shape);  // And add it to our target
                    return {};
                }
            }
            this.hit = false; //The mouse event hit air.
            if (!ev.ctrlKey){
                if(!this.target.length){
                    this.started = false;
                }
            }
            return {};
        },
        mouseup: function(ev) {
            if(!this.moved && !this.hit){
                /* We deselect objects if the event was a tap without a hit */
                this.clear();
            }
            this.mousemove(ev);
            /* Reset */ 
            this.hit = true; 
            this.moved = false;
            this.started = false;
            this.points = [];
            this.canvas.css('cursor', 'pointer');
            return {};
        },
        mousemove: function(ev) {
            if (!this.started) {
                return; //Just return if we havn't started
            }
            this.moved = true;
            var x = this.points[0].x;
            var y = this.points[0].y;
            /* We calculate the change to the coordinates */
            var xChange = ev._x0 - x;
            var yChange = ev._y0 - y;
            for (var i = 0; i < this.target.length; i++){
                /* And we apply the change to every target */
                var target = this.target[i];
                this.tools[target.type].translate(target, xChange, yChange);
            }
            /* Update our points */
            this.points[0].x = ev._x0;
            this.points[0].y = ev._y0;
            return {};
        },
        /* Clears the classes selections */
        clear: function() {
            for (var i = 0; i < this.target.length; ++i){
                delete this.target[i].format.selected;
            }
            this.target.length = 0;
        },
        /* Takes care of drawing grouped objects.  Just loops through the objects 
           and calls their corresponding draw function */
        draw: function(shape) {
            var tools = this.tools;
            //This could be the same for loop but this allows us to break
            //rather than trying to delete things that arent there.
            for (var i = 0; i < shape.points.length; ++i){
                if (shape.format.selected) {
                    shape.points[i].format.selected = true;
                } else {
                    if (shape.points[i].format.selected){
                        delete shape.points[i].format.selected;
                    } else {
                        break;
                    }
                }
            }
            for (var j = 0; j < shape.points.length; ++j){
                tools[shape.points[j].type].draw(shape.points[j]);
            }
        },
        /* Takes care of contains check for grouped objects,  just loops through
           the objects and calls their corresponding contains function */
        contains: function(shape, x, y) {
            var tools = this.tools;
            for (var i = 0; i < shape.points.length; ++i){
                if (tools[shape.points[i].type].contains(shape.points[i], x, y)){
                    return true;
                }
            }
            return false;
        },
        /* Takes care of coordinat translation for grouped objects,  just loops
           through the objects and calls their corresponding transate function */
        translate: function(shape, x, y) {
            var tools = this.tools;
            for (var i = 0; i < shape.points.length; ++i){
                tools[shape.points[i].type].translate(shape.points[i], x, y);
            }
        },
        /* Groups the selected items into a new object */
        makeGroup: function() {
            if (this.target.length < 2){
                return {};
            }
            var points = [];
            for (var i = 0; i < this.history.length;  ++i){
                if (this.history[i].format.selected){
                    points.push(this.history[i]);
                    this.history.splice(i, 1);
                    i--;
                }
            }
            this.clear();
            this.target.push({
                type: this.type,
                points: points,
                format: {selected: true}
            });
            this.history.push(this.target[0]);
            return this.target[0];
        },
        /* Ungroups the selected item */
        unGroup: function() {
            if (this.target.length === 1){
                if (this.target[0].type === 'select'){
                    this.target[0].deleted = true;
                    for (var i = 0; i < this.target[0].points.length; ++i){
                        this.history.push(this.target[0].points[i]);
                        this.target.push(this.target[0].points[i]);
                    }
                    this.target.splice(0, 1);
                }
            }

        }
    });

    /* Essentially a stamp tool,  You enter text and stamp it on the canvas */
    var Text = Tool.extend({
        name: 'Text',
        type: 'text',
        cursor: 'text',
        'formatOpts': [
            'text-color-spectrum-widget', 'text-font', 
            'text-string', 'text-size'],
        mousedown: function(ev) {
            if (!ev._format.text.string) {
                return false; //Return if there is no text
            }
            this.base(ev);
            this.points = {
                x: ev._x0,
                y: ev._y0
            };
            return {
                type: this.type,
                points: this.points,
                format: ev._format
            };
        },
        mouseup: function(ev) {
            if (!this.started) {
                return false;
            }
            return {
                type: this.type,
                points: this.base(ev),
                format: ev._format
            };

        },
        mousemove: function(ev) {
            if (!this.started) {
                return false;
            }
            this.points = {
                x: ev._x0,
                y: ev._y0
            };
            return {
                type: this.type,
                points: this.points,
                format: ev._format
            };
        },
        draw: function(shape) {
            var options = this.base(shape);
            var context = this.context;
            if (options.text.string && options.stroke.color) {
                context.beginPath();
                context.fillStyle = options.text.color;
                if (options.selected) { //Selection highlighting
                    context.globalAlpha = 0.5;
                    context.fillStyle = this.getSelectColor(options.text.color);
                }
                context.fillText(options.text.string,
                                      shape.points.x, shape.points.y);
                context.stroke();
                context.globalAlpha = 1;
            }
        },
        contains: function(shape, x, y) {
            var canvas = this.canvas;
            this.context.clearRect(0, 0, canvas.width, canvas.height);
            this.draw(shape);
            var width = this.context.measureText(shape.format.text.string).width;
            var height = parseInt(shape.format.text.size);
            var points = shape.points;
            if ((points.x <= x && x <= (points.x + width)) &&
                ((points.y - height * 0.8) <= y && y <= (points.y))) {
                return true;
            }
        },
        translate: function(shape, x, y) {
            shape.points.x += x;
            shape.points.y += y;
        }
    });

    /* 
     * A pencil tool that places a point with every mouse move 
     * and then draws lines between the points with quadratic
     * curves 
     */
    var Pencil = Tool.extend({
        name: 'Pencil',
        type: 'pencil',
        mousedown: function(ev) {
            this.base(ev);
            return {
                type: this.type,
                points: this.points,
                format: ev._format
            };
        },
        mouseup: function(ev) {
            if (!this.started) {
                return false;
            }
            var finalPoints = this.base(ev);
            return {
                type: this.type,
                points: finalPoints,
                format: ev._format
            };

        },
        mousemove: function(ev) {
            if (!this.started) {
                return false;
            }
            this.points.push({
                x: ev._x0,
                y: ev._y0
            });
            return {
                type: this.type,
                points: this.points,
                format: ev._format
            };
        },
        draw: function(shape) {
            var options = this.base(shape);
            this.context.beginPath();
            var points = shape.points;
            this.context.moveTo(points[0].x, points[0].y);
            /* takes in a context and points and draws lines as quadratic
               curves */
            function quadCurve(context, points){
                for (var i = 1; i < points.length - 2; ++i) {
                    var c = (points[i].x + points[i + 1].x) / 2;
                    var d = (points[i].y + points[i + 1].y) / 2;
                    context.quadraticCurveTo(points[i].x, points[i].y, c, d);
                }
                var len = points.length;
                context.quadraticCurveTo(
                    points[len - 2].x,
                    points[len - 2].y,
                    points[len - 1].x,
                    points[len - 1].y);
            }
            quadCurve(this.context, points);
            if (options.selected){
                this.context.globalAlpha = 0.8;
            }
            this.context.stroke();
            this.context.closePath();
            if(options.selected) { // Selection highlighting
                this.context.strokeStyle = '#00f';
                this.context.save();
                this.context.beginPath();
                this.context.lineWidth += 6;
                this.context.globalAlpha = 0.4;
                quadCurve(this.context, points);
                this.context.stroke();
                this.context.closePath();
                }
                this.context.restore();
                this.context.globalAlpha = 1;
        },
    });

    /* A base class of our shapes that extends the base tool class */
    var Shape = Tool.extend({
        mousedown: function(ev) {
            this.base(ev);
            return {
                type: this.type,
                points: this.points,
                format: ev._format
            };
        },
        mouseup: function(ev) {
            if (!this.started) {
                return false;
            }
            var finalPoints = this.base(ev);
            return {
                type: this.type,
                points: finalPoints,
                format: ev._format
            };

        },
        mousemove: function(ev) {
            if (!this.started) {
                return false;
            }
            this.points[1] = {
                x: ev._x0,
                y: ev._y0
            };
            return {
                type: this.type,
                points: this.points,
                format: ev._format
            };
        }
    });


    /* A class draws a line between the two points created by the base class */
    var Line = Shape.extend({
        name: 'Line',
        type: 'line',
        draw: function(shape) {
            var points = shape.points;
            var options = this.base(shape);
            this.context.beginPath();
            this.context.moveTo(points[0].x, points[0].y);
            this.context.lineTo(points[1].x, points[1].y);
            if (options.selected){
                this.context.globalAlpha = 0.8;
            }
            this.context.stroke();
            this.context.closePath();
            if(options.selected) {
                this.context.strokeStyle = '#00f';
                this.context.save();
                this.context.lineWidth += 6;
                this.context.globalAlpha = 0.2;
                this.context.beginPath();
                this.context.moveTo(points[0].x, points[0].y);
                this.context.lineTo(points[1].x, points[1].y);
                this.context.stroke();
                this.context.closePath();
                this.context.restore();
                this.context.globalAlpha = 1;
            }
        },
    });

    /* A class draws a rectangle between the two points created by the base class */
    var Rect = Shape.extend({
        name: 'Rectangle',
        type: 'rect',
        formatOpts: [
                'stroke-color-spectrum-widget',
                'fill-color-spectrum-widget', 'stroke-thickness'],
        draw: function(shape) {
            var points = shape.points;
            var options = this.base(shape);
            var x = Math.min(points[1].x, points[0].x);
            var y = Math.min(points[1].y, points[0].y);
            var w = Math.abs(points[1].x - points[0].x);
            var h = Math.abs(points[1].y - points[0].y);
            if (!w || !h) {
                return;
            }
            this.context.beginPath();
            if (options.selected){
                this.context.globalAlpha = 0.8;
            }
            if (options.fill.color) {
                this.context.fillRect(x, y, w, h);
            }
            if (options.stroke.color) {
                this.context.rect(x, y, w, h);
                this.context.stroke();
            }
            this.context.closePath();
            if (options.selected) {
                this.context.save();
                this.context.strokeStyle = '#00f';
                this.context.lineWidth += 8;
                this.context.globalAlpha = 0.4;
                this.context.beginPath();
                this.context.rect(x, y, w, h);
                this.context.stroke();
                this.context.closePath();
                this.context.restore();
            }
            this.context.globalAlpha = 1;
        },
        /* We extend the contains function to account for fill */
        contains: function(shape, x, y) {
            var cont = this.base(shape, x, y);
            if (!cont && shape.format.fill.color) {
                var points = shape.points;
                var xR = Math.min(points[1].x, points[0].x);
                var yR = Math.min(points[1].y, points[0].y);
                var wR = Math.abs(points[1].x - points[0].x);
                var hR = Math.abs(points[1].y - points[0].y);
                //I think i'm inverting this but too tired to check
                if ((xR <= x && x <= (xR + wR)) &&
                    (yR <= y && y <= (yR + hR))) {
                    return true;
                }
            }
            return cont;

        }
    });

    /* A class draws a circle between the two points created by the base class */
    var Circle = Shape.extend({
        name: 'Circle',
        type: 'circle',
        formatOpts: [
                'stroke-color-spectrum-widget',
                'fill-color-spectrum-widget', 'stroke-thickness'],
        draw: function(shape) {
            var points = shape.points;
            var options = this.base(shape);
            var circleCenterX = (points[0].x + points[1].x) / 2;
            var circleCenterY = (points[0].y + points[1].y) / 2;
            var radius = Math.sqrt(Math.pow(circleCenterX -
                points[0].x, 2) + Math.pow(
                circleCenterY - points[0].y, 2));

            this.context.beginPath();
            this.context.arc(circleCenterX, circleCenterY,
                radius, 0, 2 * Math.PI, false);
            if (options.selected){
                this.context.globalAlpha = 0.8;
            }
            if (options.fill.color) {
                this.context.fill();
            }
            if (options.stroke.color) {
                this.context.stroke();
            }
            this.context.closePath();
            if(options.selected) {
                this.context.save();
                this.context.strokeStyle = '#00f';
                this.context.lineWidth += 8;
                this.context.globalAlpha = 0.4;
                this.context.beginPath();
                this.context.arc(circleCenterX, circleCenterY,
                radius, 0, 2 * Math.PI, false);
                this.context.stroke();
                this.context.restore();
            } 
            this.context.globalAlpha = 1;
        },
        contains: function(shape, x, y) {
        /* We extend the contains function to account for fill */
            var cont = this.base(shape, x, y);
            if (!cont && shape.format.fill.color) {
                var points = shape.points;
                var circleCenterX = (points[0].x + points[1].x) / 2;
                var circleCenterY = (points[0].y + points[1].y) / 2;
                var radius = Math.sqrt(Math.pow(circleCenterX -
                    points[0].x, 2) + Math.pow(
                    circleCenterY - points[0].y, 2));
                var dist = Math.sqrt(Math.pow(circleCenterX -
                    x, 2) + Math.pow(circleCenterY - y, 2));
                if (dist <= radius) {
                    return true;
                }
            }
            return cont;
        },
    });
    $.fn.pWidget = function(options) {
        options.containerID = this.selector;
        var widget = new PaintingWidget(options);
        return this;
    };
})(jQuery, window, document);
